#include "foo_type.h"

int foo_new(foo_type *foo_obj)
{
  /* initialize the C struct type */
  foo_obj->val = 24;
  bar_new_trans(&foo_obj->bar_obj);
  return 0;
}

int foo_dostuff(foo_type *foo_obj)
{
  /* do some stuff */
  printf("val %d\n", foo_obj->val);
  bar_dostuff_trans(foo_obj->bar_obj);
  return 0;
}

int foo_del(foo_type *foo_obj)
{
  foo_obj->val = 0;
  bar_del_trans(&foo_obj->bar_obj);
  return 0;
}

int foo_array(const int num_foo,
              foo_type *foo_obj[])
{
  /* do some stuff on the array of pointers
     foo_obj whose size is num_foo */
  int i;
  for (i=0; i<num_foo; i++) {
    printf("idx %d: val %d\n", i, foo_obj[i]->val);
    bar_dostuff_trans(foo_obj[i]->bar_obj);
  }
  return 0;
}

int set_bar_obj(foo_type *foo_obj,
                void *bar_obj)
{
  foo_obj->bar_obj = bar_obj;
  return 0;
}

int get_bar_obj(foo_type *foo_obj,
                void **bar_obj)
{
  *bar_obj = (void *)foo_obj->bar_obj;
  return 0;
}

