#include <stdio.h>
#include <stdlib.h>

typedef struct {
  void *bar_obj;
  int val;
} foo_type;

extern int foo_new(foo_type *foo_obj);
extern int foo_dostuff(foo_type *foo_obj);
extern int foo_del(foo_type *foo_obj);
extern int foo_array(const int num_foo,
                     foo_type *foo_obj[]);

extern void bar_new_trans(void **bar_obj);
extern void bar_dostuff_trans(void *bar_obj);
extern void bar_del_trans(void **bar_obj);
extern int set_bar_obj(foo_type *foo_obj,void *bar_obj);
extern int get_bar_obj(foo_type *foo_obj,void **bar_obj);
