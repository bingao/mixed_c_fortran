#include "foo_type.h"

int foo_new_trans(void **foo_ptr)
{
  foo_type *foo_obj;
  int ierr;
  foo_obj = (foo_type *)
            malloc(sizeof(foo_type));
  if (foo_obj==NULL) exit(-1);
  ierr = foo_new(foo_obj);
  *foo_ptr = (void *)(foo_obj);
  return ierr;
}

int foo_del_trans(void **foo_ptr)
{
  foo_type *foo_obj;
  int ierr;
  foo_obj = (foo_type *)(*foo_ptr);
  ierr = foo_del(foo_obj);
  free(*foo_ptr);
  *foo_ptr = NULL;
  return ierr;
}
