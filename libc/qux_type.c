#include "qux_type.h"

int qux_new(qux_type *qux_obj)
{
  qux_obj->fun_ptr = NULL;
  return 0;
}

int qux_set_callback(
      qux_type *qux_obj,
      const callback_fun qux_callback)
{
  qux_obj->fun_ptr = qux_callback;
  return 0;
}

int qux_do_callback(qux_type *qux_obj,
                    const int num_foo,
                    foo_type *foo_obj[])
{
  qux_obj->fun_ptr(num_foo, foo_obj);
  return 0;
}

int qux_del(qux_type *qux_obj)
{
  qux_obj->fun_ptr = NULL;
  return 0;
}
