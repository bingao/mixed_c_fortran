#include <stdio.h>
#include <stdlib.h>

#include "foo_type.h"

typedef void (*callback_fun)(const int,
                             foo_type*[]);

typedef struct {
  callback_fun fun_ptr;
} qux_type;

extern int qux_new(qux_type *qux_obj);
extern int qux_set_callback(qux_type *qux_obj,
                            const callback_fun qux_callback);
extern int qux_do_callback(qux_type *qux_obj,
                           const int num_foo,
                           foo_type *foo_obj[]);
extern int qux_del(qux_type *qux_obj);
