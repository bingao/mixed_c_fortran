#include "qux_type.h"

int qux_new_trans(void **qux_ptr)
{
  qux_type *qux_obj;
  int ierr;
  qux_obj = (qux_type *)
            malloc(sizeof(qux_type));
  if (qux_obj==NULL) exit(-1);
  ierr = qux_new(qux_obj);
  *qux_ptr = (void *)(qux_obj);
  return ierr;
}

int qux_del_trans(void **qux_ptr)
{
  qux_type *qux_obj;
  int ierr;
  qux_obj = (qux_type *)(*qux_ptr);
  ierr = qux_del(qux_obj);
  free(*qux_ptr);
  *qux_ptr = NULL;
  return ierr;
}
