#include "bar_type.h"

int bar_new(bar_type *bar_obj)
{
  /* initialize the C struct type */
  bar_obj->val = 24;
  return 0;
}

int bar_dostuff(bar_type *bar_obj)
{
  /* do some stuff */
  printf("bar->val %d\n", bar_obj->val);
  return 0;
}

int bar_del(bar_type *bar_obj)
{
  bar_obj->val = 0;
  return 0;
}

int bar_array(const int num_bar,
              bar_type *bar_obj[])
{
  /* do some stuff on the array of pointers
     bar_obj whose size is num_bar */
  int i;
  for (i=0; i<num_bar; i++) {
    printf("idx %d: bar->val %d\n", i, bar_obj[i]->val);
  }
  return 0;
}
