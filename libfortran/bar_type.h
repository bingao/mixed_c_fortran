#include <stdio.h>
#include <stdlib.h>

typedef struct {
  int val;
} bar_type;

extern int bar_new(bar_type *bar_obj);
extern int bar_dostuff(bar_type *bar_obj);
extern int bar_del(bar_type *bar_obj);
extern int bar_array(const int num_bar,
                     bar_type *bar_obj[]);
