#include "bar_type.h"

int bar_new_trans(void **bar_ptr)
{
  bar_type *bar_obj;
  int ierr;
  bar_obj = (bar_type *)
            malloc(sizeof(bar_type));
  if (bar_obj==NULL) exit(-1);
  ierr = bar_new(bar_obj);
  *bar_ptr = (void *)(bar_obj);
  return ierr;
}

int bar_del_trans(void **bar_ptr)
{
  bar_type *bar_obj;
  int ierr;
  bar_obj = (bar_type *)(*bar_ptr);
  ierr = bar_del(bar_obj);
  free(*bar_ptr);
  *bar_ptr = NULL;
  return ierr;
}

//int bar_array_trans(const int num_bar,
//                      void *bar_obj[])
//{
//  int ierr;
//  ierr = bar_array(num_bar, (bar_type **)bar_obj);
//  return 0;
//}

