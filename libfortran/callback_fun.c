#include "foo_type.h"

typedef void (*callback_fun)(const int,
                             foo_type*[]);

static callback_fun callback_fun_ptr=NULL;

void callback_fun_set(const callback_fun qux_callback)
{
  callback_fun_ptr = qux_callback;
}

void callback_fun_trans(const int num_foo,
                        void *foo_ptr[])
{
  foo_type **foo_obj;
  int i;
  int ierr;
  foo_obj = (foo_type **)malloc(num_foo
          * sizeof(foo_type *));
  if (foo_obj==NULL) exit(-1);
  foo_obj[0] = (foo_type *)malloc(num_foo
             * sizeof(foo_type));
  if (foo_obj==NULL) exit(-1);
  for (i=1; i<num_foo; i++) {
    foo_obj[i] = foo_obj[i-1]+1;
  }
  ierr = foo_set_ptr_c(num_foo,
                       foo_obj,
                       foo_ptr);
  (*callback_fun_ptr)(num_foo, foo_obj);
  free(foo_obj[0]);
  free(foo_obj);
}

