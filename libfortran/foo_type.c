#include "foo_type.h"

int foo_new_c(foo_type *foo_obj)
{
  foo_new_trans(&foo_obj->ptr);
  return 0;
}

int foo_del_c(foo_type *foo_obj)
{
  foo_del_trans(&foo_obj->ptr);
  return 0;
}

int foo_dostuff_c(foo_type *foo_obj)
{
  foo_dostuff_trans(foo_obj->ptr);
  return 0;
}

int foo_array_c(const int num_foo, foo_type *foo_obj[])
{
  int i;
  void **foo_ptr;
  foo_ptr = (void **)malloc(num_foo*sizeof(void *));
  if (foo_ptr==NULL) exit(-1);
  for (i=0; i<num_foo; i++) {
    foo_ptr[i] = foo_obj[i]->ptr;
  }
  foo_ptr_array_trans(num_foo, foo_ptr);
  free(foo_ptr);
  return 0;
}

int foo_set_ptr_c(const int num_foo,
                  foo_type *foo_obj[],
                  void *foo_ptr[])
{
  int i;
  for (i=0; i<num_foo; i++) {
    foo_obj[i]->ptr = foo_ptr[i];
  }
  return 0;
}

int foo_get_ptr_c(const int num_foo,
                  foo_type *foo_obj[],
                  void *foo_ptr[])
{
  int i;
  for (i=0; i<num_foo; i++) {
    foo_ptr[i] = foo_obj[i]->ptr;
  }
  return 0;
}

int set_bar_obj_c(foo_type *foo_obj,
                  bar_type *bar_obj)
{
  set_bar_obj(foo_obj->ptr,
              (void *)bar_obj);
  return 0;
}

int get_bar_obj_c(foo_type *foo_obj,
                  bar_type **bar_obj)
{
  void *bar_ptr;
  get_bar_obj(foo_obj->ptr, &bar_ptr);
  *bar_obj = (bar_type *)bar_ptr;
  return 0;
}
