#include <stdio.h>
#include <stdlib.h>

#include "bar_type.h"

typedef struct {
  void *ptr;
} foo_type;

extern int foo_new_c(foo_type *foo_obj);
extern int foo_dostuff_c(foo_type *foo_obj);
extern int foo_del_c(foo_type *foo_obj);
extern int foo_array_c(const int num_foo, foo_type *foo_obj[]);
extern int foo_set_ptr_c(const int num_foo,
                         foo_type *foo_obj[],
                         void *foo_ptr[]);
extern int foo_get_ptr_c(const int num_foo,
                         foo_type *foo_obj[],
                         void *foo_ptr[]);

extern int set_bar_obj_c(foo_type *foo_obj,
                         bar_type *bar_obj);
extern int get_bar_obj_c(foo_type *foo_obj,
                         bar_type **bar_obj);

extern int set_bar_obj(void *foo_obj,
                       void *bar_obj);
extern int get_bar_obj(void *foo_obj,
                       void **bar_obj);

extern void foo_new_trans(void **foo_ptr);
extern void foo_del_trans(void **foo_ptr);
extern void foo_dostuff_trans(void *foo_ptr);
extern void foo_ptr_array_trans(const int num_foo,void *foo_ptr[]);
