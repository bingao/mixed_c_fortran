#include "qux_type.h"

int qux_new_c(qux_type *qux_obj)
{
  qux_new_trans(&qux_obj->ptr);
  return 0;
}

int qux_set_callback_c(qux_type *qux_obj,
                       const callback_fun qux_callback)
{
  qux_set_callback_trans(qux_obj->ptr,
                         qux_callback);
  return 0;
}

int qux_do_callback_c(qux_type *qux_obj,
                      const int num_foo,
                      foo_type *foo_obj[])
{
  int ierr;
  void **foo_ptr;
  foo_ptr = (void **)malloc(num_foo*sizeof(void *));
  if (foo_ptr==NULL) exit(-1);
  ierr = foo_get_ptr_c(num_foo,
                       foo_obj,
                       foo_ptr);
  qux_do_callback_trans(qux_obj->ptr,
                        num_foo,
                        foo_ptr);
  free(foo_ptr);
  return 0;
}

int qux_del_c(qux_type *qux_obj)
{
  qux_del_trans(&qux_obj->ptr);
  return 0;
}
