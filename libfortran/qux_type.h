#include <stdio.h>
#include <stdlib.h>

#include "foo_type.h"

typedef void (*callback_fun)(const int,
                             foo_type*[]);

typedef struct {
  void *ptr;
} qux_type;

extern int qux_new_c(qux_type *qux_obj);
extern int qux_set_callback_c(qux_type *qux_obj,
                              const callback_fun qux_callback);
extern int qux_do_callback_c(qux_type *qux_obj,
                             const int num_foo,
                             foo_type *foo_obj[]);
extern int qux_del_c(qux_type *qux_obj);

extern void qux_new_trans(void **qux_ptr);
extern void qux_set_callback_trans(void *qux_ptr,
                                   const callback_fun qux_callback);
extern void qux_do_callback_trans(void *qux_ptr,
                                  const int num_foo,
                                  void *foo_ptr[]);
extern void qux_del_trans(void **qux_ptr);
