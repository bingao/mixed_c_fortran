#include "qux_type.h"

void test_callback(const int num_foo,
                   foo_type *foo_obj[])
{
  int ierr;
  printf("test_callback>>\n");
  ierr = foo_array_c(num_foo, foo_obj);
  printf("test_callback>>\n");
}

int main()
{
#define num_foo 4
  foo_type *foo_obj[num_foo];
  bar_type *bar_obj;
  qux_type qux_obj;
  int i;
  int ierr;
  for (i=0; i<num_foo; i++) {
    foo_obj[i] = (foo_type *)malloc(sizeof(foo_type));
    ierr = foo_new_c(foo_obj[i]);
    ierr = foo_dostuff_c(foo_obj[i]);
    ierr = get_bar_obj_c(foo_obj[i], &bar_obj);
    ierr =  bar_del(bar_obj);
    ierr = set_bar_obj_c(foo_obj[i], bar_obj);
  }
  ierr = foo_array_c(num_foo, foo_obj);
  ierr = qux_new_c(&qux_obj);
  ierr = qux_set_callback_c(&qux_obj, test_callback);
  ierr = qux_do_callback_c(&qux_obj, num_foo, foo_obj);
  ierr = qux_del_c(&qux_obj);
  for (i=0; i<num_foo; i++) {
    ierr = foo_del_c(foo_obj[i]);
    free(foo_obj[i]);
    foo_obj[i] = NULL;
  }
  return 0;
}
